#include "main.h"

#include "samp_pimpl.hpp"

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	if ( SAMP::isR1() ) {
		nop_markers.changeAddr( 0xB119E );
		nop_markers2.changeAddr( 0xB11A7 );
		nop_markers3.changeAddr( 0xB1270 );
	}
	nop_markers.enable();
	nop_markers2.enable();
	nop_markers3.enable();
}

AsiPlugin::~AsiPlugin() = default;
