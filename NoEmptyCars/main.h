#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

#include "Patch.h"

class AsiPlugin : public SRDescent {
	SRHook::Patch nop_markers{ 0xB6E5E, { 0xB8, 0x01, 0x00, 0x00, 0x00 }, "samp" };
	SRHook::Patch nop_markers2{ 0xB6E67, { 0xEB }, "samp" };
	SRHook::Patch nop_markers3{ 0xB6F30, { 0xEB }, "samp" };

public:
	explicit AsiPlugin();
	~AsiPlugin() override;
};

#endif // MAIN_H
