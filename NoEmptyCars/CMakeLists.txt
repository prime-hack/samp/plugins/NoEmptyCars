cmake_minimum_required(VERSION 3.15)

get_filename_component(PROJECT_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${PROJECT_NAME} LANGUAGES CXX)
set(CMAKE_SHARED_MODULE_PREFIX "../")
set(CMAKE_SHARED_MODULE_SUFFIX ".asi")

include(asi_options)

aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR} ${PROJECT_NAME}_LIST)
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/loader ${PROJECT_NAME}_LIST)

add_library(${PROJECT_NAME} MODULE ${${PROJECT_NAME}_LIST})

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
	CXX_VISIBILITY_PRESET hidden
	C_VISIBILITY_PRESET hidden
	VISIBILITY_INLINES_HIDDE ON
)

target_compile_definitions(${PROJECT_NAME} PRIVATE PROJECT_NAME_C="${PROJECT_NAME}")

target_link_libraries(${PROJECT_NAME} PRIVATE llmo SRDescent ${Boost_LIBRARIES} samp)
if (EXISTS ${CMAKE_SOURCE_DIR}/third-party/SRCursor)
	target_link_libraries(${PROJECT_NAME} PRIVATE SRCursor)
endif()
if (EXISTS ${CMAKE_SOURCE_DIR}/third-party/SREvents)
	target_link_libraries(${PROJECT_NAME} PRIVATE SREvents)
endif()
if(DXHOOK)
	target_link_libraries(${PROJECT_NAME} PRIVATE ProxyDX9)
endif()
if(DRAWHOOK)
	target_link_libraries(${PROJECT_NAME} PRIVATE DrawHook)
endif()
if(DXHOOK OR DRAWHOOK)
	target_link_libraries(${PROJECT_NAME} PRIVATE render)
endif()

include(asi_compile_flags OPTIONAL)

include(asi_postbuild_prepare OPTIONAL)

install(TARGETS ${PROJECT_NAME} DESTINATION ./)
